// строка с информацией о ребенке
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:testapp/model/objects/child.dart';
import 'package:testapp/values/app_colors.dart';
import 'package:testapp/values/app_dimensions.dart';

import '../screens/employee_card/bloc/bloc_employee_card.dart';
import '../screens/employee_card/bloc/events_employee_card.dart';


class ChildListItem extends StatelessWidget{

  final Child child;
  ChildListItem(this.child);

  @override
  Widget build(BuildContext context) {
    final String _fullName = (child?.family ?? "")
        + " " + (child?.name ?? "")
        + " " + (child?.middleName ?? "");
    return InkWell(
      onTap: () => BlocProvider
          .of<BlocEmployeeCard>(context)
          .add(EventOnChildClicked(child)),
      child: Padding(
        padding: const EdgeInsets.symmetric(vertical: 8),
        child: Row(
          mainAxisSize: MainAxisSize.max,
          children: [
            Padding(
              padding: const EdgeInsets.symmetric(
                  horizontal: AppDimensions.HORIZONTAL_PADDING),
              child: Icon(Icons.person_outline, size: 32,),
            ),
            Expanded(
              child: Column(
                mainAxisSize: MainAxisSize.min,
                crossAxisAlignment: CrossAxisAlignment.start,
                children: <Widget>[
                  Text(_fullName),
                  Text(child.birthday, style: TextStyle(color: AppColors.GREY),),
                ],
              ),
            )
          ],
        ),
      ),
    );
  }
}
