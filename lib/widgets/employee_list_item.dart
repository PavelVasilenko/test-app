// строка с информацией о сотруднике
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:testapp/model/objects/employee.dart';
import 'package:testapp/values/app_colors.dart';
import 'package:testapp/values/app_dimensions.dart';

import '../screens/employees/bloc/bloc_employee.dart';
import '../screens/employees/bloc/events_employee.dart';

class EmployeeListItem extends StatelessWidget{

  final Employee employee;
  EmployeeListItem(this.employee);

  @override
  Widget build(BuildContext context) {
    final String _fullName = (employee?.family ?? "")
        + " " + (employee?.name ?? "")
        + " " + (employee?.middleName ?? "");

    return InkWell(
      onTap: () => BlocProvider
          .of<BlocEmployee>(context)
          .add(EventOnEmployeeClicked(employee)),
      child: Padding(
        padding: const EdgeInsets.symmetric(vertical: 8),
        child: Row(
          mainAxisSize: MainAxisSize.max,
          children: [
            Padding(
              padding: const EdgeInsets.symmetric(
                  horizontal: AppDimensions.HORIZONTAL_PADDING),
              child: Icon(Icons.person_outline, size: 32,),
            ),
            Expanded(
              child: Column(
                mainAxisSize: MainAxisSize.min,
                crossAxisAlignment: CrossAxisAlignment.start,
                children: <Widget>[
                  Text(_fullName),
                  Text(employee?.birthday ?? "", style: TextStyle(color: AppColors.GREY),),
                  Text(employee?.position ?? "", style: TextStyle(color: AppColors.GREY),),
                ],
              ),
            )
          ],
        ),
      ),
    );
  }
}
