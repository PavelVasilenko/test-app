import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:testapp/values/app_colors.dart';
import 'package:testapp/values/app_dimensions.dart';

class ThemeTextField extends StatelessWidget {
  final String hint;
  final TextInputType keyboardType;
  final bool isWrongValue;
  final TextEditingController textController;
  final TextCapitalization capitalization;
  final Function onTapCallBack;

  ThemeTextField({
    this.textController,
    this.keyboardType = TextInputType.text,
    this.hint = "",
    this.isWrongValue,
    this.capitalization,
    this.onTapCallBack,
  });

  @override
  Widget build(BuildContext context) {

    return TextFormField(
        keyboardType: keyboardType,
        cursorColor: AppColors.GREY,
        controller: textController,
        readOnly: onTapCallBack != null,

        onTap: () {
          if(onTapCallBack != null) onTapCallBack();
        },
        textCapitalization: capitalization ?? TextCapitalization.none,
        decoration: new InputDecoration(
          focusColor: AppColors.GREY,
          contentPadding: EdgeInsets.only(
              left: AppDimensions.RADIUS,
              right: AppDimensions.RADIUS,
              top: 12.0,
              bottom: 12.0),
          focusedBorder: UnderlineInputBorder(
            borderSide: BorderSide(
                color: (isWrongValue != null && isWrongValue) ?
                  AppColors.ERROR : AppColors.ACCENT,
                width: 1.5),
          ),
          enabledBorder: UnderlineInputBorder(
            borderSide: BorderSide(
                color: (isWrongValue != null && isWrongValue) ?
                  AppColors.ERROR : AppColors.GREY,
                width: 1.5),
          ),
          errorBorder: UnderlineInputBorder(
            borderSide: BorderSide(
                color: AppColors.ERROR,
                width: 1.5),
          ),
          filled: false,
          hintText: hint ?? "",
          fillColor: AppColors.APP_BACKGROUND));
  }
}
