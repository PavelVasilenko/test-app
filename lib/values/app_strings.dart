
class AppStrings{

  static const String APP_NAME = "Мои сотрудники";

  static const String EMPLOYEES_CAPTION = "Сотрудники";
  static const String EMPLOYEES_EMPTY_LIST = "Список сотрудников пуст";

  static const String EMPLOYEE_CARD_CAPTION = "Данные сотрудника";
  static const String EMPLOYEE_NEW_CARD_CAPTION = "Новый сотрудник";
  static const String EMPLOYEE_FAMILY = "Фамилия";
  static const String EMPLOYEE_NAME = "Имя";
  static const String EMPLOYEE_MIDDLE_NAME = "Отчество";
  static const String EMPLOYEE_POSITION = "Должность";
  static const String EMPLOYEE_BIRTH_DAY = "Дата рождения";
  static const String EMPLOYEE_CHILDREN = "Дети:";
  static const String EMPLOYEE_ADD_CHILD = "ДОБАВИТЬ РЕБЁНКА";

  static const String CHILDREN_CAPTION = "Дети сотрудника";
  static const String CHILDREN_EMPTY_LIST = "Список детей пуст";

  static const String CHILD_CARD_CAPTION = "Данные ребёнка";
  static const String CHILD_NEW_CARD_CAPTION = "Новый ребёнок";
  static const String CHILD_FAMILY = "Фамилия";
  static const String CHILD_NAME = "Имя";
  static const String CHILD_MIDDLE_NAME = "Отчество";
  static const String CHILD_BIRTH_DAY = "Дата рождения";

  static const String ERROR_EMPTY_FIELDS = "Ошибка. Все поля должны быть заполнены";
  static const String COMMIT_DELETE_CAPTION = "Подтверждение";
  static const String COMMIT_DELETE_TEXT = "Вы действительно хотите удалить запись?";
  static const String BUT_YES = "ДА";
  static const String BUT_NO = "НЕТ";

}