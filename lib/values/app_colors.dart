
import 'package:flutter/material.dart';

class AppColors{
  static const Color APP_BACKGROUND = Color(0xffFFFFFF);
  static const Color ACCENT = Color(0xff668dcc);
  static const Color ERROR = Color(0xffff0000);
  static const Color GREY = Color(0xffD0D0D0);
}
