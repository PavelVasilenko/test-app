

import 'package:testapp/model/objects/child.dart';

abstract class StatesChildCard{}

class StateUninitializedChild extends StatesChildCard{}

class StateShowDatePicker extends StatesChildCard{
  final DateTime initialDateTime;
  StateShowDatePicker(this.initialDateTime);
}

class StateFinishWithResult extends StatesChildCard{
  var result;
  StateFinishWithResult(this.result);
}

class StateChildCard extends StatesChildCard{
  final Child child;
  final bool isNewChild;
  final bool updateAllFields;
  final bool updateBirthday;
  final bool errorInFamily;
  final bool errorInName;
  final bool errorInMiddleName;
  final bool errorInBirthday;

  StateChildCard(this.child, {
    this.isNewChild,
    this.updateAllFields = false,
    this.updateBirthday = false,
    this.errorInFamily,
    this.errorInName,
    this.errorInMiddleName,
    this.errorInBirthday,
  });
}

