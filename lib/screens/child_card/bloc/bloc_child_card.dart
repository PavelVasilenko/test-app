import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:testapp/model/formatter.dart';
import 'package:testapp/model/objects/child.dart';
import 'package:testapp/screens/child_card/child_screen.dart';

import 'events_child_card.dart';
import 'states_child_card.dart';

class BlocChildCard extends Bloc<EventsChildCard, StatesChildCard>{

  // редактируемый ребёнок
  Child _child;

  bool _isNewChild;

  @override
  StatesChildCard get initialState => StateUninitializedChild();

  @override
  Stream<StatesChildCard> mapEventToState(EventsChildCard event) async*{

    // инициализация данных
    if(event is EventInitChildCard) {
      yield* _mapInitChildToState(event);
    }

    // выбрана дата рождения
    else if(event is EventOnDateSelected) {
      yield* _mapOnDateSelectedToState(event);
    }

    // нажато поле дата рождения
    else if(event is EventOnBirthdayClick) {
      yield* _mapBirthdayClickToState();
    }

    // сохранение данных
    else if(event is EventSaveClicked) {
      yield* _mapSaveClickedToState(event);
    }

    // удаление записи подтверждно
    else if(event is EventChildDeleteConfirmed) {
      yield StateFinishWithResult(true);
    }

  }

  // ---------------------------------------------------------------------------
  // сохранение данных
  Stream<StatesChildCard> _mapSaveClickedToState(
      EventSaveClicked event) async* {
      bool wrongFamily = (event.family == null || event.family.isEmpty);
      bool wrongName = (event.name == null || event.name.isEmpty);
      bool wrongMiddleName = (event.middleName == null || event.middleName.isEmpty);
      bool wrongBirthday = (event.birthday == null || event.birthday.isEmpty);

      if(wrongFamily || wrongName || wrongMiddleName || wrongBirthday) {
        yield StateChildCard(
          _child,
          isNewChild: _isNewChild,
          errorInFamily: wrongFamily,
          errorInName: wrongName,
          errorInMiddleName: wrongMiddleName,
          errorInBirthday: wrongBirthday,
        );
        return;
      }

      _child.family = event.family;
      _child.name = event.name;
      _child.middleName = event.middleName;
      _child.birthday = event.birthday;

      yield StateFinishWithResult(_child);
  }

  // ---------------------------------------------------------------------------
  // если пользователь выбрал дату рождения
  Stream<StatesChildCard> _mapOnDateSelectedToState(
      EventOnDateSelected event) async* {
    _child.birthday = Formatter.dateToString(event.dateTime);
    yield StateChildCard(
        _child,
        isNewChild: _isNewChild,
        updateBirthday: true);
  }

  // ---------------------------------------------------------------------------
  // нажато поле дата рождения
  Stream<StatesChildCard> _mapBirthdayClickToState() async* {
    DateTime _parsedDateTime = DateTime
        .tryParse(_child.birthday ?? "") ?? DateTime.now();
    yield StateShowDatePicker(_parsedDateTime);
  }

  // ---------------------------------------------------------------------------
  // инициализация данных
  Stream<StatesChildCard> _mapInitChildToState(EventInitChildCard event) async* {

    // проверка входный данных
    if(event.arguments != null && event.arguments is! Map<String, dynamic>)
      throw(UnimplementedError("Incorrect init arguments"));

    Map<String, dynamic> arguments = event.arguments;

    if(arguments[ChildScreen.ARGUMENT_FIELD_PARENT_ID] is! int)
      throw(UnimplementedError("Incorrect init arguments"));

    if(arguments.containsKey(ChildScreen.ARGUMENT_FIELD_CHILD) &&
        arguments[ChildScreen.ARGUMENT_FIELD_CHILD] is Child) {
      _child = arguments[ChildScreen.ARGUMENT_FIELD_CHILD];
      _isNewChild = false;
    } else {
      _child = Child();
      _isNewChild = true;
    }
    // это или карточка редактирования ребёенка
    // или карточка нового ребёнка

    yield StateChildCard(
        _child,
        isNewChild: _isNewChild,
        updateAllFields: true);
  }

}