abstract class EventsChildCard{}

class EventOnBirthdayClick extends EventsChildCard{}

class EventChildDeleteConfirmed extends EventsChildCard{}

class EventSaveClicked extends EventsChildCard{
  final String family;
  final String name;
  final String middleName;
  final String birthday;
  EventSaveClicked(this.family, this.name, this.middleName, this.birthday);
}

class EventInitChildCard extends EventsChildCard{
  var arguments;
  EventInitChildCard(this.arguments);
}

class EventOnDateSelected extends EventsChildCard{
  DateTime dateTime;
  EventOnDateSelected(this.dateTime);
}
