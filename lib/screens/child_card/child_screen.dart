import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:testapp/screens/child_card/bloc/bloc_child_card.dart';
import 'package:testapp/screens/child_card/bloc/states_child_card.dart';
import 'package:testapp/values/app_colors.dart';
import 'package:testapp/values/app_dimensions.dart';
import 'package:testapp/values/app_strings.dart';
import 'package:testapp/widgets/theme_text_field.dart';

import 'bloc/events_child_card.dart';

class ChildScreen extends StatelessWidget {
  static const String ARGUMENT_FIELD_PARENT_ID = 'parentId';
  static const String ARGUMENT_FIELD_CHILD = 'childData';

  final TextEditingController _textControllerFamily = TextEditingController();
  final TextEditingController _textControllerName = TextEditingController();
  final TextEditingController _textControllerMiddleName =
      TextEditingController();
  final TextEditingController _textControllerBirthday = TextEditingController();

  final _scaffoldKey = GlobalKey<ScaffoldState>();

  // ---------------------------------------------------------------------------
  Future<void> _showConfirmDialog(BuildContext blocContext) async {
    showDialog(
        context: blocContext,
        builder: (ctx) => AlertDialog(
          title: Text(AppStrings.COMMIT_DELETE_CAPTION),
          content: Text(AppStrings.COMMIT_DELETE_TEXT),
          actions: [
            FlatButton(
                child: Text(AppStrings.BUT_NO),
                onPressed: () => Navigator.of(blocContext).pop()),
           FlatButton(
                child: Text(AppStrings.BUT_YES),
                onPressed: () {
                  Navigator.of(blocContext).pop();
                  BlocProvider.of<BlocChildCard>(blocContext)
                    .add(EventChildDeleteConfirmed());
                })
          ],
        ));
  }
  
  // ---------------------------------------------------------------------------
  // построение содержимого страницы
  Widget _pageContent(BuildContext blocContext, StateChildCard state) {

    List<Widget> _actionsList = List();
    if(!state.isNewChild) {
      _actionsList.add(
        InkWell(
          onTap: () => _showConfirmDialog(blocContext),
          child: Padding(
            padding: const EdgeInsets.all(16.0),
            child: Icon(Icons.delete_outline, color: Colors.white,),
          ),
        )
      );
    }

    return Scaffold(
      key: _scaffoldKey,
      appBar: AppBar(
        actions: _actionsList,
        title: state.isNewChild
          ? Text(AppStrings.CHILD_NEW_CARD_CAPTION)
          : Text(AppStrings.CHILD_CARD_CAPTION),
      ),
      body: SingleChildScrollView(
        child: Padding(
          padding: const EdgeInsets.symmetric(
              horizontal: AppDimensions.HORIZONTAL_PADDING, vertical: 16.0),
          child: Column(
            mainAxisSize: MainAxisSize.min,
            crossAxisAlignment: CrossAxisAlignment.stretch,
            children: [
              ThemeTextField(
                hint: AppStrings.CHILD_FAMILY,
                capitalization: TextCapitalization.sentences,
                textController: _textControllerFamily,
                isWrongValue: state.errorInFamily != null && state.errorInFamily,
              ),
              Container(height: 4.0),
              ThemeTextField(
                hint: AppStrings.CHILD_NAME,
                capitalization: TextCapitalization.sentences,
                textController: _textControllerName,
                isWrongValue: state.errorInName != null && state.errorInName,
              ),
              Container(height: 4.0),
              ThemeTextField(
                hint: AppStrings.CHILD_MIDDLE_NAME,
                capitalization: TextCapitalization.sentences,
                textController: _textControllerMiddleName,
                isWrongValue: state.errorInMiddleName != null && state.errorInMiddleName,
              ),
              Container(height: 4.0),
              ThemeTextField(
                hint: AppStrings.CHILD_BIRTH_DAY,
                capitalization: TextCapitalization.sentences,
                textController: _textControllerBirthday,
                onTapCallBack: () => BlocProvider.of<BlocChildCard>(blocContext)
                    .add(EventOnBirthdayClick()),
                isWrongValue: state.errorInBirthday != null && state.errorInBirthday,
              ),
              Container(height: 16.0),
            ],
          ),
        ),
      ),
      floatingActionButton: FloatingActionButton(
          child: Icon(Icons.done),
          onPressed: () => BlocProvider.of<BlocChildCard>(blocContext)
            .add(EventSaveClicked(
              _textControllerFamily.text,
              _textControllerName.text,
              _textControllerMiddleName.text,
              _textControllerBirthday.text,
          ))),
    );
  }

  // ---------------------------------------------------------------------------
  // построение страницы с сожиданием
  Widget _pagePending() {
    return Scaffold(
      appBar: AppBar(
        title: Text(AppStrings.CHILD_CARD_CAPTION),
      ),
      body: Container(
        child: Center(
          child: CircularProgressIndicator(),
        ),
      ),
    );
  }

  // ---------------------------------------------------------------------------
  // определение true = listen или false = build
  bool _blocConsumerType(StatesChildCard state) =>
      state is StateShowDatePicker || state is StateFinishWithResult;

  // ---------------------------------------------------------------------------
  // отображение каледаря для выбора даты рождения
  Future<void> _showDatePicker(
      BuildContext blocContext, DateTime selectedDate) async {
    DateTime result = await showDatePicker(
      context: blocContext,
      initialDate: selectedDate ?? DateTime.now(),
      firstDate: DateTime.utc(1900),
      lastDate: DateTime.utc(2100),
      locale: Locale('ru', 'RU'),
      helpText: AppStrings.CHILD_BIRTH_DAY,
      initialEntryMode: DatePickerEntryMode.calendar,
    );
    if (result != null) {
      BlocProvider.of<BlocChildCard>(blocContext)
        .add(EventOnDateSelected(result));
    }
  }

  // ---------------------------------------------------------------------------
  bool anyErrorInFields(StateChildCard state) {
    return(state.errorInBirthday == true || state.errorInMiddleName == true ||
        state.errorInName == true || state.errorInFamily == true);
  }

  // ---------------------------------------------------------------------------
  void showSnackBar(String text) {
    _scaffoldKey.currentState.showSnackBar(
        SnackBar(
            content: Text(text),
          backgroundColor: AppColors.ACCENT,
        )
    );
  }

  // ---------------------------------------------------------------------------
  @override
  Widget build(BuildContext context) {
    return BlocProvider<BlocChildCard>(
      create: (context) => BlocChildCard(),
      child: BlocConsumer<BlocChildCard, StatesChildCard>(
        buildWhen: (oldState, newState) => !_blocConsumerType(newState),
        listenWhen: (oldState, newState) => _blocConsumerType(newState),
        listener: (blocContext, state) {
          // закрытие экрана с параметром
          if (state is StateFinishWithResult) {
            Navigator.of(blocContext)
                .pop(state.result);
            return;
          }
          // отображение каледаря для выбора даты рождения
          if (state is StateShowDatePicker) {
            _showDatePicker(blocContext, state.initialDateTime);
            return;
          }
        },
        builder: (blocContext, state) {
          // построение экрана

          // данные не проинициализированы
          if (state is StateUninitializedChild) {
            // передаём в блок аргументы
            BlocProvider.of<BlocChildCard>(blocContext).add(
              EventInitChildCard(
                ModalRoute.of(blocContext).settings.arguments));
            return _pagePending();
          }

          // состояние с данными о ребёнке
          if (state is StateChildCard) {

            // если нужно обновить дату
            if (state.updateBirthday != null && state.updateBirthday) {
              _textControllerBirthday.text = state.child.birthday;
            }

            // если нужно обновить все поля
            if (state.updateAllFields != null && state.updateAllFields) {
              _textControllerFamily.text = state.child.family;
              _textControllerName.text = state.child.name;
              _textControllerMiddleName.text = state.child.middleName;
              _textControllerBirthday.text = state.child.birthday;
            }


            if(anyErrorInFields(state)) {
              WidgetsBinding.instance.addPostFrameCallback((timeStamp) {
                showSnackBar(AppStrings.ERROR_EMPTY_FIELDS);
              });
            }

            return _pageContent(blocContext, state);
          }
          // неизвестное состояние
          throw UnimplementedError("Unknown child card state.");
        },
      ),
    );
  }
}
