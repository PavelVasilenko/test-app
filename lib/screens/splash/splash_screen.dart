import 'package:flutter/material.dart';
import 'package:testapp/main.dart';
import 'package:testapp/model/db.dart';
import 'package:testapp/values/app_strings.dart';

class SplashScreen extends StatefulWidget{
  @override
  State createState() => SplashScreenState();
}

class SplashScreenState extends State<SplashScreen>{

  @override
  void initState() {
    _prepareModel();
    super.initState();
  }

  // инициализация данных
  Future<void> _prepareModel() async {
    await Db.inst.init();
    Navigator.of(context).pushReplacementNamed(MyApp.ROUTE_MAIN_SCREEN);
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Container(
        child: Center(
          child: Text(
            AppStrings.APP_NAME,
            style: TextStyle(
              fontSize: 24,
              fontWeight: FontWeight.bold
            ),
          ),
        ),
      ),
    );
  }
}