import 'package:testapp/model/objects/employee.dart';

abstract class StatesEmployee{}

class StateUninitialized extends StatesEmployee{}

class StateNavigateToScreen extends StatesEmployee{
  String  routeName;
  var     arguments;
  StateNavigateToScreen(this.routeName, {this.arguments});
}

class StateEmployeesList extends StatesEmployee{
  final List<Employee> employees;
  StateEmployeesList(this.employees);
}