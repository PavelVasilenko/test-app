import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:testapp/main.dart';
import 'package:testapp/model/db.dart';
import 'package:testapp/model/objects/employee.dart';
import 'package:testapp/screens/employees/bloc/events_employee.dart';
import 'package:testapp/screens/employees/bloc/states_employee.dart';

class BlocEmployee extends Bloc<EventsEmployee, StatesEmployee>{

  @override
  StatesEmployee get initialState => StateUninitialized();

  @override
  Stream<StatesEmployee> mapEventToState(EventsEmployee event) async* {

    // получение списка сотрудников из базы
    if(event is EventUpdateEmployeesList) {
      yield* _mapUpdateEmployeesListToState();
      return;
    }

    // добавление нового сотрудника в базу
    else if(event is EventNewEmployeeClicked) {
      yield StateNavigateToScreen(MyApp.ROUTE_EMPLOYEE_CARD_SCREEN);
      return;
    }

    // пришел ответ от вызванного экрана
    else if(event is EventOnResponseFromScreen) {
      yield* _mapResponseFromScreenToState(event);
    }

    // клик на ребенке в списке
    else if(event is EventOnEmployeeClicked) {
      yield* _mapOnEmployeeClickedToState(event);
    }

  }

  // ---------------------------------------------------------------------------
  // нажатие на строчку с сотрудником
  Stream<StatesEmployee> _mapOnEmployeeClickedToState(
      EventOnEmployeeClicked event) async* {
    yield StateNavigateToScreen(MyApp.ROUTE_EMPLOYEE_CARD_SCREEN,
      arguments: event.employee);
  }

  // ---------------------------------------------------------------------------
  // получение списка сотрудников из базы
  Stream<StatesEmployee> _mapUpdateEmployeesListToState() async* {
    List<Employee> employeesList = await Db.inst.getEmployeesAsync();
    yield StateEmployeesList(employeesList);
  }

  // ---------------------------------------------------------------------------
  // пришел ответ от вызванного экрана
  Stream<StatesEmployee> _mapResponseFromScreenToState(
      EventOnResponseFromScreen event) async* {

    // пришел ответ от окна с данными о сотруднике
    if(event.routeName.compareTo(MyApp.ROUTE_EMPLOYEE_CARD_SCREEN) == 0) {
      if(event.result != null) {
        List<Employee> employeesList = await Db.inst.getEmployeesAsync();
        yield StateEmployeesList(employeesList);
      }
    }

  }

  // ---------------------------------------------------------------------------

}