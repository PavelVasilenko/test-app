
import 'package:testapp/model/objects/employee.dart';

abstract class EventsEmployee{}

// нажато на кнопку добавления нового сотрудника
class EventNewEmployeeClicked extends EventsEmployee{}

// обновление списка сотрудников
class EventUpdateEmployeesList extends EventsEmployee{}

// нажато на кнопку просмотра  сотрудника
class EventOnEmployeeClicked extends EventsEmployee{
  final Employee employee;
  EventOnEmployeeClicked(this.employee);
}

class EventOnResponseFromScreen extends EventsEmployee{
  String  routeName;
  var     arguments;
  var     result;
  EventOnResponseFromScreen(this.routeName, this.arguments, this.result);
}
