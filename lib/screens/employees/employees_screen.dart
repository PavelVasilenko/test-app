import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:testapp/values/app_strings.dart';

import '../../widgets/employee_list_item.dart';
import 'bloc/bloc_employee.dart';
import 'bloc/events_employee.dart';
import 'bloc/states_employee.dart';

class EmployeesScreen extends StatelessWidget {

  // построение содержимого страницы
  Widget _pageContent(BuildContext blocContext, StateEmployeesList state) {
    Widget _content;
    if(state.employees == null || state.employees.isEmpty) {
      // если список сотрудников пуст
      _content = Container(
        child: Center(
          child: Text(
            AppStrings.EMPLOYEES_EMPTY_LIST
          )
        ),
      );
    } else {
      // отображение списка сотрудников
      _content = ListView.builder(
        itemBuilder: (BuildContext context, int index) {
          return EmployeeListItem(state.employees[index]);
        },
        itemCount: state.employees.length,
      );
    }

    return Scaffold(
      appBar: AppBar(
        title: Text(AppStrings.EMPLOYEES_CAPTION),
      ),
      body: _content,
      floatingActionButton: FloatingActionButton(
        child: Icon(Icons.add),
        onPressed: () => BlocProvider
            .of<BlocEmployee>(blocContext)
            .add(EventNewEmployeeClicked())
      ),
    );
  }

  // ---------------------------------------------------------------------------
  // построение страницы с сожиданием
  Widget _pagePending() {
    return Scaffold(
      appBar: AppBar(
        title: Text(AppStrings.EMPLOYEES_CAPTION),
      ),
      body: Container(
        child: Center(
          child: CircularProgressIndicator(),
        ),
      ),
    );
  }

  // ---------------------------------------------------------------------------
  Future<void> _navigate(blocContext, state) async {
    var result = await Navigator.of(blocContext)
        .pushNamed(state.routeName, arguments: state.arguments);
    if(result != null)
      BlocProvider.of<BlocEmployee>(blocContext).add(
          EventOnResponseFromScreen(
              state.routeName,
              state.arguments,
              result)
      );
  }

  // ---------------------------------------------------------------------------
  @override
  Widget build(BuildContext context) {
    return BlocProvider<BlocEmployee>(
      create: (context) => BlocEmployee(),
      child: BlocConsumer<BlocEmployee, StatesEmployee>(
        buildWhen: (oldState, newState) => newState is! StateNavigateToScreen,
        listenWhen: (oldState, newState) => newState is StateNavigateToScreen,
        listener: (blocContext, state) {
          // навигация на другой экран
          if(state is StateNavigateToScreen) {
            _navigate(blocContext, state);
          }
        },
        builder: (context, state) {
          // построение экрана

          // данные не проинициализированы
          if(state is StateUninitialized) {
            BlocProvider.of<BlocEmployee>(context).add(EventUpdateEmployeesList());
            return _pagePending();
          }

          // состояние с прочитанным списком сотрудников
          if(state is StateEmployeesList) {
            return _pageContent(context, state);
          }
          // неизвестное состояние
          throw UnimplementedError("Unknown employees state.");
        },
      ),
    );
  }

}

