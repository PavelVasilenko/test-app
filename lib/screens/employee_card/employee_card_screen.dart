import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:testapp/values/app_colors.dart';

import 'package:testapp/widgets/child_list_item.dart';
import 'package:testapp/values/app_dimensions.dart';
import 'package:testapp/values/app_strings.dart';
import 'package:testapp/widgets/theme_text_field.dart';

import 'bloc/bloc_employee_card.dart';
import 'bloc/states_employee_card.dart';
import 'bloc/events_employee_card.dart';

class EmployeeCardScreen extends StatelessWidget {
  final TextEditingController _textControllerFamily = TextEditingController();
  final TextEditingController _textControllerName = TextEditingController();
  final TextEditingController _textControllerMiddleName =
      TextEditingController();
  final TextEditingController _textControllerPosition = TextEditingController();
  final TextEditingController _textControllerBirthday = TextEditingController();

  final _scaffoldKey = GlobalKey<ScaffoldState>();

  // ---------------------------------------------------------------------------
  Widget _personalFields(BuildContext blocContext, StateEmployeeCard state) {
    return Padding(
      padding: const EdgeInsets.symmetric(
          horizontal: AppDimensions.HORIZONTAL_PADDING, vertical: 16.0),
      child: Column(
        mainAxisSize: MainAxisSize.min,
        crossAxisAlignment: CrossAxisAlignment.stretch,
        children: [
          ThemeTextField(
            hint: AppStrings.EMPLOYEE_FAMILY,
            capitalization: TextCapitalization.sentences,
            textController: _textControllerFamily,
            isWrongValue: state.errorInFamily != null && state.errorInFamily,
          ),
          Container(height: 4.0),
          ThemeTextField(
            hint: AppStrings.EMPLOYEE_NAME,
            capitalization: TextCapitalization.sentences,
            textController: _textControllerName,
            isWrongValue: state.errorInName != null && state.errorInName,
          ),
          Container(height: 4.0),
          ThemeTextField(
            hint: AppStrings.EMPLOYEE_MIDDLE_NAME,
            capitalization: TextCapitalization.sentences,
            textController: _textControllerMiddleName,
            isWrongValue: state.errorInMiddleName != null && state.errorInMiddleName,
          ),
          Container(height: 4.0),
          ThemeTextField(
            hint: AppStrings.EMPLOYEE_POSITION,
            capitalization: TextCapitalization.sentences,
            textController: _textControllerPosition,
            isWrongValue: state.errorInPosition != null && state.errorInPosition,
          ),
          Container(height: 4.0),
          ThemeTextField(
            hint: AppStrings.EMPLOYEE_BIRTH_DAY,
            capitalization: TextCapitalization.sentences,
            textController: _textControllerBirthday,
            onTapCallBack: () => BlocProvider
                .of<BlocEmployeeCard>(blocContext)
                .add(EventOnBirthdayClick()),
            isWrongValue: state.errorInBirthday != null && state.errorInBirthday,
          ),
          Container(height: 16.0),

          Row(
            children: [
              Text(AppStrings.EMPLOYEE_CHILDREN),
              Spacer(),
              FlatButton(
                child: Text(AppStrings.EMPLOYEE_ADD_CHILD, style: TextStyle(color: AppColors.ACCENT),),
                onPressed: () => BlocProvider
                    .of<BlocEmployeeCard>(blocContext)
                    .add(EventNewChildClicked()),
              ),
            ],
          ),
        ],
      ),
    );
  }

  // ---------------------------------------------------------------------------
  // построение содержимого страницы
  Widget _pageContent(BuildContext blocContext, StateEmployeeCard state) {


    List<Widget> _actionsList = List();
    if(!state.employee.isNewEmployee) {
      _actionsList.add(
          InkWell(
            onTap: () => _showConfirmDialog(blocContext),
            child: Padding(
              padding: const EdgeInsets.all(16.0),
              child: Icon(Icons.delete_outline, color: Colors.white,),
            ),
          )
      );
    }

    return Scaffold(
      key: _scaffoldKey,
      body: CustomScrollView(slivers: [
        SliverAppBar(
          floating: true,
          actions: _actionsList,
          title: state.employee.isNewEmployee
              ? Text(AppStrings.EMPLOYEE_NEW_CARD_CAPTION)
              : Text(AppStrings.EMPLOYEE_CARD_CAPTION),
        ),
        SliverList(
          delegate: SliverChildBuilderDelegate(
            (BuildContext context, int index) {
              if (index == 0) {
                return _personalFields(blocContext, state);
              } else {
                return ChildListItem(state.children[index - 1]);
              }
            },
            childCount: 1 + (state.children?.length ?? 0),
          ),
        ),
      ]),
      floatingActionButton: FloatingActionButton(
          child: Icon(Icons.done),
          onPressed: () => BlocProvider.of<BlocEmployeeCard>(blocContext)
              .add(EventSaveClicked(
                _textControllerFamily.text,
                _textControllerName.text,
                _textControllerMiddleName.text,
                _textControllerBirthday.text,
                _textControllerPosition.text,
          ))),
    );
  }

  // ---------------------------------------------------------------------------
  Future<void> _showConfirmDialog(BuildContext blocContext) async {
    showDialog(
        context: blocContext,
        builder: (ctx) => AlertDialog(
          title: Text(AppStrings.COMMIT_DELETE_CAPTION),
          content: Text(AppStrings.COMMIT_DELETE_TEXT),
          actions: [
            FlatButton(
                child: Text(AppStrings.BUT_NO),
                onPressed: () => Navigator.of(blocContext).pop()),
            FlatButton(
                child: Text(AppStrings.BUT_YES),
                onPressed: () {
                  Navigator.of(blocContext).pop();
                  BlocProvider.of<BlocEmployeeCard>(blocContext)
                      .add(EventEmployeeDeleteConfirmed());
                })
          ],
        ));
  }

  // ---------------------------------------------------------------------------
  // построение страницы с сожиданием
  Widget _pagePending() {
    return Scaffold(
      appBar: AppBar(
        title: Text(AppStrings.EMPLOYEE_CARD_CAPTION),
      ),
      body: Container(
        child: Center(
          child: CircularProgressIndicator(),
        ),
      ),
    );
  }

  // ---------------------------------------------------------------------------
  // определение true = listen или false = build
  bool _blocConsumerType(StatesEmployeeCard state) =>
      state is StateNavigateToScreen ||
      state is StateFinishWithResult ||
      state is StateShowDatePicker;

  // ---------------------------------------------------------------------------
  // отображение каледаря для выбора даты рождения
  Future<void> _showDatePicker(BuildContext blocContext, DateTime selectedDate) async {
    DateTime result = await showDatePicker (
      context: blocContext,
      initialDate: selectedDate ?? DateTime.now(),
      firstDate: DateTime.utc(1900),
      lastDate: DateTime.utc(2100),
      locale: Locale('ru', 'RU'),
      helpText: AppStrings.EMPLOYEE_BIRTH_DAY,
      initialEntryMode: DatePickerEntryMode.calendar,
    );
    if(result != null) {
      BlocProvider
          .of<BlocEmployeeCard>(blocContext)
          .add(EventOnDateSelected(result));
    }
  }

  // ---------------------------------------------------------------------------
  Future<void> _navigate(blocContext, state) async {
    var result = await Navigator.of(blocContext)
        .pushNamed(state.routeName, arguments: state.arguments);
    if(result != null)
      BlocProvider.of<BlocEmployeeCard>(blocContext).add(
        EventOnResponseFromScreen(
            state.routeName,
            state.arguments,
            result)
      );
  }


  // ---------------------------------------------------------------------------
  bool anyErrorInFields(StateEmployeeCard state) {
    return(state.errorInBirthday == true || state.errorInMiddleName == true ||
        state.errorInName == true || state.errorInFamily == true ||
        state.errorInPosition == true);
  }

  // ---------------------------------------------------------------------------
  void showSnackBar(String text) {
    _scaffoldKey.currentState.showSnackBar(
        SnackBar(
          content: Text(text),
          backgroundColor: AppColors.ACCENT,
        )
    );
  }

  // ---------------------------------------------------------------------------
  @override
  Widget build(BuildContext context) {
    return BlocProvider<BlocEmployeeCard>(
      create: (context) => BlocEmployeeCard(),
      child: BlocConsumer<BlocEmployeeCard, StatesEmployeeCard>(
        buildWhen: (oldState, newState) => !_blocConsumerType(newState),
        listenWhen: (oldState, newState) => _blocConsumerType(newState),
        listener: (blocContext, state) {
          // навигация на другой экран
          if (state is StateNavigateToScreen) {
            _navigate(blocContext, state);
            return;
          }

          // отображение каледаря для выбора даты рождения
          if (state is StateShowDatePicker) {
            _showDatePicker(blocContext, state.initialDateTime);
            return;
          }

          // закрытие экрана с параметром
          if (state is StateFinishWithResult) {
            Navigator.of(blocContext)
                .pop(state.result);
            return;
          }

        },
        builder: (blocContext, state) {
          // построение экрана

          // данные не проинициализированы
          if (state is StateUninitializedEmployee) {
            // передаём в блок аргументы
            BlocProvider.of<BlocEmployeeCard>(blocContext).add(
                EventInitEmployeeCard(
                    ModalRoute.of(blocContext).settings.arguments));
            return _pagePending();
          }

          // состояние с данными о сотруднике
          if (state is StateEmployeeCard) {
            if(state.updateBirthday != null && state.updateBirthday) {
              _textControllerBirthday.text = state.employee.birthday;
            }

            // если нужно обновить все поля
            if (state.updateAllFields != null && state.updateAllFields) {
              _textControllerFamily.text = state.employee.family;
              _textControllerName.text = state.employee.name;
              _textControllerMiddleName.text = state.employee.middleName;
              _textControllerBirthday.text = state.employee.birthday;
              _textControllerPosition.text = state.employee.position;
            }

            if(anyErrorInFields(state)) {
              WidgetsBinding.instance.addPostFrameCallback((timeStamp) {
                showSnackBar(AppStrings.ERROR_EMPTY_FIELDS);
              });
            }

            return _pageContent(blocContext, state);
          }
          // неизвестное состояние
          throw UnimplementedError("Unknown employee card state.");
        },
      ),
    );
  }
}
