
import 'package:testapp/model/objects/child.dart';

abstract class EventsEmployeeCard{}

class EventNewChildClicked extends EventsEmployeeCard{}

class EventOnBirthdayClick extends EventsEmployeeCard{}

class EventEmployeeDeleteConfirmed extends EventsEmployeeCard{}

class EventSaveClicked extends EventsEmployeeCard{
  final String family;
  final String name;
  final String middleName;
  final String birthday;
  final String position;
  EventSaveClicked(this.family, this.name, this.middleName,
      this.birthday, this.position);
}


class EventInitEmployeeCard extends EventsEmployeeCard{
  var arguments;
  EventInitEmployeeCard(this.arguments);
}

class EventOnDateSelected extends EventsEmployeeCard{
  DateTime dateTime;
  EventOnDateSelected(this.dateTime);
}

class EventOnChildClicked extends EventsEmployeeCard{
  Child child;
  EventOnChildClicked(this.child);
}

class EventOnResponseFromScreen extends EventsEmployeeCard{
  String  routeName;
  var     arguments;
  var     result;
  EventOnResponseFromScreen(this.routeName, this.arguments, this.result);
}
