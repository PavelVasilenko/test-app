import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:testapp/main.dart';
import 'package:testapp/model/db.dart';
import 'package:testapp/model/formatter.dart';
import 'package:testapp/model/objects/child.dart';
import 'package:testapp/model/objects/employee.dart';
import 'package:testapp/screens/child_card/child_screen.dart';

import 'events_employee_card.dart';
import 'states_employee_card.dart';

class BlocEmployeeCard extends Bloc<EventsEmployeeCard, StatesEmployeeCard>{

  // редактируемый сотрудник
  Employee _employee;

  // дети сотрудника
  List<Child> _children;

  @override
  StatesEmployeeCard get initialState => StateUninitializedEmployee();

  @override
  Stream<StatesEmployeeCard> mapEventToState(EventsEmployeeCard event) async*{

    // инициализация данных
    if(event is EventInitEmployeeCard) {
      yield* _mapInitEmployeeToState(event);
    }

    // нажата кнопка добавления ребенка
    else if(event is EventNewChildClicked) {
      yield* _mapNewChildToState();
    }

    // клик на ребенке в списке
    else if(event is EventOnChildClicked) {
      yield* _mapOnChildClickedToState(event);
    }

    // выбрана дата рождения
    else if(event is EventOnDateSelected) {
      yield* _mapOnDateSelectedToState(event);
    }

    // нажато поле дата рождения
    else if(event is EventOnBirthdayClick) {
      yield* _mapBirthdayClickToState();
    }

    // пришел ответ от вызванного экрана
    else if(event is EventOnResponseFromScreen) {
      yield* _mapResponseFromScreenToState(event);
    }

    // нажата кнопка сохранение
    else if(event is EventSaveClicked) {
      yield* _mapSaveClickedToState(event);
    }

    // удаление записи подтверждно
    else if(event is EventEmployeeDeleteConfirmed) {
      yield* _mapEmployeeDeleteToState();
    }

  }

  // ---------------------------------------------------------------------------
  // удаление данных о сотруднике
  Stream<StatesEmployeeCard> _mapEmployeeDeleteToState() async* {

    await Db.inst.deleteEmployeeAsync(_employee.id);
    yield StateFinishWithResult(true);
  }

  // ---------------------------------------------------------------------------
  // нажата кнопка сохранение
  Stream<StatesEmployeeCard> _mapSaveClickedToState(
      EventSaveClicked event) async* {

    bool wrongFamily = (event.family == null || event.family.isEmpty);
    bool wrongName = (event.name == null || event.name.isEmpty);
    bool wrongMiddleName = (event.middleName == null || event.middleName.isEmpty);
    bool wrongBirthday = (event.birthday == null || event.birthday.isEmpty);
    bool wrongPosition = (event.position == null || event.position.isEmpty);

    if(wrongFamily || wrongName || wrongMiddleName ||
        wrongBirthday || wrongPosition) {
      yield StateEmployeeCard(_employee, _children,
        errorInFamily: wrongFamily,
        errorInName: wrongName,
        errorInMiddleName: wrongMiddleName,
        errorInBirthday: wrongBirthday,
        errorInPosition: wrongPosition,
      );
      return;
    }

    _employee.family = event.family;
    _employee.name = event.name;
    _employee.middleName = event.middleName;
    _employee.birthday = event.birthday;
    _employee.position = event.position;

    if(_employee.isNewEmployee) {
      _employee.id = await Db.inst.addEmployeeAsync(_employee);
    } else {
      await Db.inst.updateEmployeeAsync(_employee);
    }

    await Db.inst.updateChildrenAsync(_employee.id, _children);

    yield StateFinishWithResult(true);

  }

  // ---------------------------------------------------------------------------
  // нажатие на строчку с ребенком
  Stream<StatesEmployeeCard> _mapOnChildClickedToState(
      EventOnChildClicked event) async* {
    var arguments = {
      ChildScreen.ARGUMENT_FIELD_PARENT_ID: _employee.id,
      ChildScreen.ARGUMENT_FIELD_CHILD: event.child
    };
    yield StateNavigateToScreen(MyApp.ROUTE_CHILD_SCREEN, arguments: arguments);
  }

  // ---------------------------------------------------------------------------
  void _sortChildrenByFamily() {
    _children.sort((c1, c2) => c1.family.compareTo(c2.family));
  }

  // ---------------------------------------------------------------------------
  // пришел ответ от вызванного экрана
  Stream<StatesEmployeeCard> _mapResponseFromScreenToState(
      EventOnResponseFromScreen event) async* {

    // пришел ответ от окна с данными о ребенке
    if(event.routeName.compareTo(MyApp.ROUTE_CHILD_SCREEN) == 0) {
      // в экран передавались данные
      if(event.arguments != null && event.arguments is Map<String, dynamic>) {

        Map<String, dynamic> args = event.arguments;

        if(args.containsKey(ChildScreen.ARGUMENT_FIELD_CHILD)) {
          // это было редактирование ребёнка
          Child item = args[ChildScreen.ARGUMENT_FIELD_CHILD];

          if(event.result is bool && event.result) {
            // удаление записи
            _children.remove(item);
          }

          _sortChildrenByFamily();
          yield StateEmployeeCard(_employee, _children);
          return;

        } else {
          // это было добавление нового ребёнка
          if(event.result is Child) {
            // ребёнок был добавлен
            _children.add(event.result);
            _sortChildrenByFamily();
            yield StateEmployeeCard(_employee, _children);
            return;
          }
        }
      }
    }

  }

  // ---------------------------------------------------------------------------
  // если пользователь выбрал дату рождения
  Stream<StatesEmployeeCard> _mapOnDateSelectedToState(
      EventOnDateSelected event) async* {
    _employee.birthday = Formatter.dateToString(event.dateTime);
    yield StateEmployeeCard(_employee, _children, updateBirthday: true);
  }

  // ---------------------------------------------------------------------------
  // нажато поле дата рождения
  Stream<StatesEmployeeCard> _mapBirthdayClickToState() async* {
    DateTime _parsedDateTime = DateTime
        .tryParse(_employee.birthday ?? "") ?? DateTime.now();
    yield StateShowDatePicker(_parsedDateTime);
  }

  // ---------------------------------------------------------------------------
  // нажата кнопка добавления ребенка
  Stream<StatesEmployeeCard> _mapNewChildToState() async* {
    var arguments = {
      ChildScreen.ARGUMENT_FIELD_PARENT_ID : _employee.id
    };
    yield StateNavigateToScreen(MyApp.ROUTE_CHILD_SCREEN, arguments: arguments);
  }

  // ---------------------------------------------------------------------------
  // инициализация данных
  Stream<StatesEmployeeCard> _mapInitEmployeeToState(EventInitEmployeeCard event) async* {
    var arguments = event.arguments;

    // проверка входный данных
    if(arguments != null && arguments is! Employee)
      throw(UnimplementedError("Init argument is not Employee"));

    // это или карточка редактирования старого сотрудника
    // или карточка нового сотрудника
    _employee = arguments ?? Employee();

    // получение списка детей
    if(_employee.isNewEmployee) {
      // новый пустой список
      _children = List<Child>();
    } else {
      // список детей из базы
      _children = await Db.inst.getChildrenAsync(_employee.id);
    }

    yield StateEmployeeCard(_employee, _children, updateAllFields: true);
  }

}