
import 'package:testapp/model/objects/child.dart';
import 'package:testapp/model/objects/employee.dart';

abstract class StatesEmployeeCard{}

class StateUninitializedEmployee extends StatesEmployeeCard{}

class StateShowDatePicker extends StatesEmployeeCard{
  final DateTime initialDateTime;
  StateShowDatePicker(this.initialDateTime);
}

class StateEmployeeCard extends StatesEmployeeCard{
  final Employee employee;
  final List<Child> children;
  final bool updateAllFields;
  final bool updateBirthday;
  final bool errorInFamily;
  final bool errorInName;
  final bool errorInMiddleName;
  final bool errorInPosition;
  final bool errorInBirthday;
  StateEmployeeCard(
      this.employee,
      this.children, {
        this.updateBirthday = false,
        this.updateAllFields = false,
        this.errorInFamily,
        this.errorInName,
        this.errorInMiddleName,
        this.errorInPosition,
        this.errorInBirthday,
      });
}

class StateNavigateToScreen extends StatesEmployeeCard{
  String  routeName;
  var     arguments;
  StateNavigateToScreen(this.routeName, {this.arguments});
}

class StateFinishWithResult extends StatesEmployeeCard{
  var result;
  StateFinishWithResult(this.result);
}

