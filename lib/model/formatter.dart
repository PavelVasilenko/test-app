import 'package:intl/intl.dart';

class Formatter{

  static String dateToString(DateTime dateTime) {
    var formatter = new DateFormat('yyyy-MM-dd');
    return formatter.format(dateTime);
  }
}