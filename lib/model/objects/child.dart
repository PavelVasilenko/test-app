
// Ребёнок
class Child {
//  static const int NEW_CHILD_ID = -1;

  int    id; // id в базе
  int    parentId; // id родителя в базе
  String family; // Фамилия
  String name; // Имя
  String middleName; // Отчество
  String birthday; // Дата рождения

  Child.fill(
      this.id,
      this.family,
      this.name,
      this.middleName,
      this.birthday,
      this.parentId,
      );

  Child(){
//    id = NEW_CHILD_ID;
  }

//  bool get isNewChild => id == NEW_CHILD_ID;
}
