
// Сотрудник
class Employee {

  static const int NEW_EMPLOYEE_ID = -1;

  int    id; // id в базе
  String family; // Фамилия
  String name; // Имя
  String middleName; // Отчество
  String birthday; // Дата рождения
  String position; // Должность

  Employee.fill(
    this.id,
    this.family,
    this.name,
    this.middleName,
    this.birthday,
    this.position,
  );

  Employee(){
    id = NEW_EMPLOYEE_ID;
  }

  bool get isNewEmployee => id == NEW_EMPLOYEE_ID;
}