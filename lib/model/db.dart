
import 'package:sqflite/sqflite.dart';
import 'package:path/path.dart';
import 'package:testapp/model/objects/child.dart';

import 'objects/employee.dart';

// База сторудников и их детей
class Db {
  Database _database;
  // ---------------------------------------------------------------------------
  // Singleton
  Db._privateConstructor();
  static final Db _inst = Db._privateConstructor();
  static Db get inst => _inst;
  // ---------------------------------------------------------------------------

  static const int  DB_VERSION = 1;

  static const String EMPLOYEE =            "employees";
  static const String EMPLOYEE_KEY =        "id";
  static const String EMPLOYEE_FAMILY =     "family";
  static const String EMPLOYEE_NAME =       "name";
  static const String EMPLOYEE_MIDDLE_NAME= "middlename";
  static const String EMPLOYEE_POSITION =   "position";
  static const String EMPLOYEE_BIRTH_DAY =  "birthday";

  static const String CHILD =               "childrens";
  static const String CHILD_KEY =           "id";
  static const String CHILD_FAMILY =        "family";
  static const String CHILD_NAME =          "name";
  static const String CHILD_MIDDLE_NAME=    "middlename";
  static const String CHILD_BIRTH_DAY =     "birthday";
  static const String CHILD_PARENT_ID =     "parent";

  // инициализация
  Future<void> init() async {
    if(_database != null) return;

    var databasesPath = await getDatabasesPath();

    String path = join(databasesPath, 'testapp.db');
    _database = await openDatabase(path, version: DB_VERSION,
      onCreate: (Database db, int version) async {
        await db.execute(
          "CREATE TABLE $EMPLOYEE ("
            "id INTEGER PRIMARY KEY, "
            "$EMPLOYEE_FAMILY TEXT, "
            "$EMPLOYEE_NAME TEXT, "
            "$EMPLOYEE_MIDDLE_NAME TEXT, "
            "$EMPLOYEE_POSITION TEXT, "
            "$EMPLOYEE_BIRTH_DAY TEXT"
          ")"
        );
        await db.execute(
          "CREATE TABLE $CHILD ("
            "$CHILD_KEY INTEGER PRIMARY KEY, "
            "$CHILD_FAMILY TEXT, "
            "$CHILD_NAME TEXT, "
            "$CHILD_MIDDLE_NAME TEXT, "
            "$CHILD_BIRTH_DAY TEXT, "
            "$CHILD_PARENT_ID INTEGER"
          ")"
        );
      });
  }

  // ---------------------------------------------------------------------------
  // добавление сотрудника
  Future<int> addEmployeeAsync(Employee employee) async {
    return await _database.rawInsert("INSERT INTO $EMPLOYEE("
        "$EMPLOYEE_FAMILY, $EMPLOYEE_NAME, $EMPLOYEE_MIDDLE_NAME,"
        "$EMPLOYEE_POSITION, $EMPLOYEE_BIRTH_DAY) VALUES ("
        "?, ?, ?, ?, ?)",[
      employee.family ?? "", employee.name ?? "", employee.middleName ?? "",
      employee.position ?? "", employee.birthday ?? ""
    ]);
  }

  // ---------------------------------------------------------------------------
  // изменение сотрудника
  Future<int> updateEmployeeAsync(Employee employee) async {
    return await _database.rawUpdate("UPDATE $EMPLOYEE SET "
        "$EMPLOYEE_FAMILY = ?, "
        "$EMPLOYEE_NAME = ?, "
        "$EMPLOYEE_MIDDLE_NAME = ?, "
        "$EMPLOYEE_POSITION = ?, "
        "$EMPLOYEE_BIRTH_DAY = ? "
        "WHERE $EMPLOYEE_KEY = ?",[
      employee.family ?? "", employee.name ?? "", employee.middleName ?? "",
      employee.position ?? "", employee.birthday ?? "", employee.id
    ]);
  }

  // ---------------------------------------------------------------------------
  // удаление сотрудника
  Future<void> deleteEmployeeAsync(int employeeId) async {
    return await _database.transaction<void>((txn) async {
      await txn.rawDelete("DELETE FROM $EMPLOYEE WHERE $EMPLOYEE_KEY = ?", [
        employeeId
      ]);
      await txn.rawDelete("DELETE FROM $CHILD WHERE $CHILD_PARENT_ID = ?", [
        employeeId
      ]);
    });
  }

  // ---------------------------------------------------------------------------
  // получение списка сотрудников
  Future<List<Employee>> getEmployeesAsync() async {
    List<Map> maps = await _database.rawQuery(
        'SELECT * FROM $EMPLOYEE ORDER BY $EMPLOYEE_FAMILY '
    );
    List<Employee> ret = List.generate(maps.length, (i) =>
        Employee.fill(
            maps[i][EMPLOYEE_KEY],
            maps[i][EMPLOYEE_FAMILY],
            maps[i][EMPLOYEE_NAME],
            maps[i][EMPLOYEE_MIDDLE_NAME],
            maps[i][EMPLOYEE_BIRTH_DAY],
            maps[i][EMPLOYEE_POSITION],
        )
    );
    return ret;
  }
  
  // ---------------------------------------------------------------------------
  // ---------------------------------------------------------------------------
  // ---------------------------------------------------------------------------
  
  // обновление всех детей у сотрудника
  Future<void> updateChildrenAsync(int parentId, List<Child> children) async {

    if(parentId == null) throw UnimplementedError("parentId is null");

    return await _database.transaction<void>((txn) async {
      await txn.rawDelete("DELETE FROM $CHILD "
          "WHERE $CHILD_PARENT_ID = ?",[parentId]);
      for(Child child in children) {
        await txn.rawInsert("INSERT INTO $CHILD("
            "$CHILD_FAMILY, $CHILD_NAME, $CHILD_MIDDLE_NAME,"
            "$CHILD_PARENT_ID, $CHILD_BIRTH_DAY) VALUES ("
            "?, ?, ?, ?, ?)",[
          child.family ?? "", child.name ?? "", child.middleName ?? "",
          parentId, child.birthday ?? ""
        ]);
      }
    });
  }


  // ---------------------------------------------------------------------------
  // получение списка детей
  Future<List<Child>> getChildrenAsync(int parentId) async {
    List<Child> ret;
    try {
      List<Map> maps = await _database.rawQuery(
          'SELECT * FROM $CHILD WHERE $CHILD_PARENT_ID = ? ORDER BY $CHILD_FAMILY ',
          [
            parentId
          ]);
      ret = List.generate(maps.length, (i) =>
          Child.fill(
            maps[i][CHILD_KEY],
            maps[i][CHILD_FAMILY],
            maps[i][CHILD_NAME],
            maps[i][CHILD_MIDDLE_NAME],
            maps[i][CHILD_BIRTH_DAY],
            parentId,
          )
      );
    } catch(e) {
      print(e);
    }
    return ret;
  }

  // ---------------------------------------------------------------------------


}
