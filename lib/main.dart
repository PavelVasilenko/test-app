import 'package:flutter/material.dart';
import 'package:flutter_localizations/flutter_localizations.dart';

import 'screens/child_card/child_screen.dart';
import 'screens/employee_card/employee_card_screen.dart';
import 'screens/employees/employees_screen.dart';
import 'screens/splash/splash_screen.dart';
import 'values/app_colors.dart';
import 'values/app_strings.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {

  static const String ROUTE_SPLASH = '/';
  static const String ROUTE_MAIN_SCREEN = '/employees';
  static const String ROUTE_EMPLOYEE_CARD_SCREEN = '/employee_card';
  static const String ROUTE_CHILD_SCREEN = '/children';

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      title: AppStrings.APP_NAME,
      localizationsDelegates: [
        GlobalMaterialLocalizations.delegate,
        GlobalWidgetsLocalizations.delegate,
      ],
      supportedLocales: [
        const Locale('ru', 'RU'),
      ],
      theme: ThemeData(
          primaryColor: AppColors.ACCENT,
          accentColor: AppColors.ACCENT,
          scaffoldBackgroundColor: AppColors.APP_BACKGROUND),
      routes: {
        ROUTE_SPLASH: (context) => SplashScreen(),
        ROUTE_MAIN_SCREEN: (context) => EmployeesScreen(),
        ROUTE_EMPLOYEE_CARD_SCREEN: (context) => EmployeeCardScreen(),
        ROUTE_CHILD_SCREEN: (context) => ChildScreen(),
      },
    );
  }
}
